﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Business.Implementation;
using WebShop.DTO;

namespace WebShop.Website.Controllers
{
    public class ProductController : Controller
    {
        ProductManager productManager = ProductManagerBuilder.GetProductManager();

        // GET: Product
        public ActionResult Index()
        {            
            return View(productManager.GetAll());
        }

        public ActionResult SwitchStorageType()
        {
            StorageManager.Instance.Switch();
            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(ProductDTO product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    productManager.Add(product);
                }
                else
                {
                    ViewBag.ErrorMessage = "Product has some invalid data";
                    return View();
                }                
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }

            return RedirectToAction("Index");
        }

    }
}