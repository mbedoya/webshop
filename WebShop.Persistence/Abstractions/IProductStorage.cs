﻿using System.Collections.Generic;
using WebShop.DTO;

namespace WebShop.Storage.Abstractions
{
    public interface IProductStorage
    {
        IReadOnlyList<ProductDTO> GetAll();
        void Add(ProductDTO product);
    }
}
