﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using WebShop.DTO;
using WebShop.Storage.Abstractions;

namespace WebShop.Storage.Implementation.ProductStorage
{
    public class CSVProductStorage : IProductStorage
    {
        private List<ProductDTO> products;
        private const string appSettingsFilePathKey = "CsvStorageFilePath";
        private string filePath;
        private const char fieldSeparator = ',';
        private const char categorySeparator = '|';
        private const string lineBreak = "\r\n";

        private ProductDTO ConvertCsvLineToProduct(string line)
        {
            string[] fields = line.Split(fieldSeparator);

            ProductDTO product = new ProductDTO();
            product.Title = fields[0];
            product.Number = fields[1];
            product.Price = Convert.ToDecimal(fields[2]);

            if (fields.Length > 3)
            {
                product.Categories = new List<string>(fields[3].Split(categorySeparator));
            }            

            return product;
        }

        private string ConvertProductToCSVLine(ProductDTO product)
        {
            string line = String.Empty;

            if (product.Categories != null && product.Categories.Count > 0)
            {
                line = String.Format("{0}" + fieldSeparator.ToString() + "{1}" + fieldSeparator.ToString() + "{2}" + fieldSeparator.ToString() + "{3}",
                product.Title, product.Number, product.Price.ToString(), String.Join(categorySeparator.ToString(), product.Categories.ToArray()));
            }
            else
            {
                line = String.Format("{0}" + fieldSeparator.ToString() + "{1}" + fieldSeparator.ToString() + "{2}" + fieldSeparator.ToString(),
                product.Title, product.Number, product.Price.ToString());
            }

            return line;
        }

        private void LoadSavedData()
        {
            if (File.Exists(filePath))
            {
                string[] lines = File.ReadAllLines(filePath);

                foreach (var item in lines)
                {
                    products.Add(ConvertCsvLineToProduct(item));
                }
            }
        }

        public CSVProductStorage()
        {
            filePath = ConfigurationManager.AppSettings[appSettingsFilePathKey];

            products = new List<ProductDTO>();

            LoadSavedData();
        }

        public void Add(ProductDTO product)
        {
            File.AppendAllText(filePath, lineBreak + ConvertProductToCSVLine(product));
        }

        public IReadOnlyList<ProductDTO> GetAll()
        {
            return products;
        }
    }
}
