﻿using System;
using System.Collections.Generic;
using WebShop.DTO;
using WebShop.Storage.Abstractions;

namespace WebShop.Storage.Implementation.ProductStorage
{
    public class InMemoryProductStorage : IProductStorage
    {

        private static InMemoryProductStorage instance;
        private static object syncRoot = new Object();

        public static InMemoryProductStorage Instance
        {
            get
            {

                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new InMemoryProductStorage();
                    }
                }

                return instance;
            }
        }

        private List<ProductDTO> products;

        private void AddSampleData()
        {
            ProductDTO product1 = new ProductDTO() { Title = "Felt AR1 Road Bike", Price = 8441 };
            product1.Categories = new List<string>() { "Felt", "Road Bike" };
            products.Add(product1);

            ProductDTO product2 = new ProductDTO() { Title = "Oakley Jawbreaker Matte Black", Price = 137 };
            product2.Categories = new List<string>() { "Oakley", "Road Bike", "Jawbreaker" };
            products.Add(product2);
        }

        private InMemoryProductStorage()
        {
            products = new List<ProductDTO>();

            AddSampleData();
        }

        public void Add(ProductDTO product)
        {
            products.Add(product);
        }

        public IReadOnlyList<ProductDTO> GetAll()
        {            

            return products;
        }
    }
}
