﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebShop.DTO
{
    public class ProductDTO
    {
        [Required(ErrorMessage = "Product title must not be empty")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Product number must not be empty")]
        public string Number { get; set; }

        public decimal Price { get; set; }

        public List<string> Categories { get; set; }
    }
}
