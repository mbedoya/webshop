﻿using WebShop.Storage.Implementation.ProductStorage;

namespace WebShop.Business.Implementation
{
    public class ProductManagerBuilder
    {
        public static ProductManager GetProductManager()
        {
            ProductManager product;

            if (StorageManager.Instance.GetStorageType() == StorageType.InMemory)
            {
                product = new ProductManager(InMemoryProductStorage.Instance);
            }
            else
            {
                product = new ProductManager(new CSVProductStorage());
            }

            return product;
        }
    }
}
