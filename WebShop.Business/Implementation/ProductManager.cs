﻿using System;
using System.Collections.Generic;
using WebShop.DTO;
using WebShop.Storage.Abstractions;

namespace WebShop.Business.Implementation
{
    public class ProductManager {

        private IProductStorage productStorage;

        private const char categorySeparator = ',';

        private void Validate(ProductDTO product)
        {
            if (String.IsNullOrEmpty(product.Title))
            {
                throw new Exception("Product title must not be empty");
            }

            if (String.IsNullOrEmpty(product.Number))
            {
                throw new Exception("Product number must not be empty");
            }

            if (product.Price < 0)
            {
                throw new Exception("Product price nust not be negative");
            }            
        }

        public ProductManager(IProductStorage productStorage)
        {
            this.productStorage = productStorage;
        }

        public void ChangeProductStorage(IProductStorage productStorage)
        {
            this.productStorage = productStorage;
        }

        public IReadOnlyList<ProductDTO> GetAll()
        {
            return productStorage.GetAll();
        }

        private List<string> ParseCategories(List<string> categories)
        {
            if (categories != null && categories.Count == 1)
            {
                return new List<string>(categories[0].Split(categorySeparator));
            }

            return categories;
        }

        public void Add(ProductDTO product)
        {
            Validate(product);

            product.Categories = ParseCategories(product.Categories);
            productStorage.Add(product);
        }
    }
}
