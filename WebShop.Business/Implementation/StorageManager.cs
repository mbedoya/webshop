﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebShop.Business.Implementation
{
    public class StorageManager
    {
        private static StorageManager instance;
        private static object syncRoot = new Object();

        private StorageType selectedStorageType;

        public static StorageManager Instance {
            get
            {

                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new StorageManager();
                    }
                }

                return instance;
            }
        }

        private StorageManager()
        {
            selectedStorageType = StorageType.InMemory;
        }

        public void SetStorageType(StorageType storageType)
        {
            selectedStorageType = storageType;
        }

        public void Switch()
        {
            if (selectedStorageType == StorageType.InMemory)
            {
                selectedStorageType = StorageType.XMLPersistence;
            }
            else
            {
                selectedStorageType = StorageType.InMemory;
            }
        }

        public StorageType GetStorageType()
        {
            return selectedStorageType;
        }
    }

    public enum StorageType
    {
        InMemory,
        XMLPersistence
    }
}
