﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebShop.Business.Implementation;
using WebShop.DTO;
using WebShop.Storage.Abstractions;

namespace WebShop.Business.Tests
{
    [TestFixture]
    public class ProductManagerTest
    {
        Mock<IProductStorage> stubProductManager;
        ProductManager productManager;
        ProductDTO product;

        [SetUp]
        public void Init()
        {
            stubProductManager = new Mock<IProductStorage>();
            productManager = new ProductManager(stubProductManager.Object);
            product = new ProductDTO();
        }

        [Test]
        public void Add_EmptyTitleSent_ThrowsException()
        {
            Assert.Throws<Exception>(() => productManager.Add(product));
        }

        [Test]
        public void Add_EmptyNumberSent_ThrowsException()
        {
            product.Title = "Product Title";

            Assert.Throws<Exception>(() => productManager.Add(product));
        }

        [Test]
        public void Add_NegativePriceSent_ThrowsException()
        {
            product.Title = "Product Title";
            product.Number = "2010";
            product.Price = -1;

            Assert.Throws<Exception>(() => productManager.Add(product));
        }
    }
}
