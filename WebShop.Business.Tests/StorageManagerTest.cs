﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebShop.Business.Implementation;

namespace WebShop.Business.Tests
{
    [TestFixture]
    public class StorageManagerTest
    {
        [Test]
        public void Switch_WorksLikeSingleton_SameValueForAllInstances()
        {
            StorageManager storageManager = StorageManager.Instance;
            StorageManager storageManager2 = StorageManager.Instance;

            StorageType instance1Type = storageManager.GetStorageType();            
            StorageType instance2Type = storageManager.GetStorageType();

            Assert.AreEqual(instance1Type, instance2Type);

            storageManager2.Switch();

            instance1Type = storageManager.GetStorageType();
            instance2Type = storageManager.GetStorageType();

            Assert.AreEqual(instance1Type, instance2Type);
        }

        [Test]
        public void Switch_InstanceValueChanged_ValueChanged()
        {
            StorageManager storageManager = StorageManager.Instance;
            storageManager.SetStorageType(StorageType.InMemory);

            storageManager.Switch();
            StorageType instanceType = storageManager.GetStorageType();

            Assert.AreEqual(StorageType.XMLPersistence, instanceType);
        }
    }
}
